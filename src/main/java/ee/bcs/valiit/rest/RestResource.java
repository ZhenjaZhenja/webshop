package ee.bcs.valiit.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import ee.bcs.valiit.model.Book;
import ee.bcs.valiit.model.Transaction;
import ee.bcs.valiit.model.User;
import ee.bcs.valiit.services.AuthenticationService;
import ee.bcs.valiit.services.BookService;
import ee.bcs.valiit.services.FileService;
import ee.bcs.valiit.services.TransactionService;
import ee.bcs.valiit.services.UserService;

@Path("/")
public class RestResource {

	@GET
	@Path("/get_books")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getBooks() {
		return BookService.getBooks();
	}
	
	@GET
	@Path("/get_book_details")
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBook(@QueryParam("book_id") int bookId) {
		return BookService.getBook(bookId);
	}
	
	@GET
	@Path("/get_my_books")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getMyBooks(@QueryParam("owner_id") int ownerId) {
		return BookService.getMyBooks(ownerId);
	}
	
	@GET
	@Path("/get_my_transaction")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Transaction> getMyBorrowedBooks(@QueryParam("borrower_id") int borrowerId) {
		return TransactionService.getMyBorrowedBooks(borrowerId);
	}


	
	@POST
	@Path("/add_order")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addOrder(Transaction transaction) {
		TransactionService.addOrder(transaction);
		TransactionService.updateOrder(transaction.getBookId());
		return "OK";
	}

	
//	@POST
//	@Path("/add_book")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.TEXT_PLAIN)
//	public String addBook(Book book) {
//		BookService.addBook(book);
//		return "OK";
//	}	
	
	@POST
	@Path("/add_book")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public String addBook(
			@Context HttpServletRequest req,
			@FormDataParam("id") int id,
			@FormDataParam("isbn") String isbn, 
			@FormDataParam("title") String title,
			@FormDataParam("author") String author, 
			@FormDataParam("publisher") String publisher,
			@FormDataParam("publishingYear") int publishingYear,
			@FormDataParam("pageCount") int pageCount,
			@FormDataParam("weight") double weight, 
			@FormDataParam("dimensions") String dimensions,
			@FormDataParam("fee") double fee, 
			@FormDataParam("description") String description, 
			@FormDataParam("langauge") String language,
			@FormDataParam("photoUrl") InputStream photoUrl,
			@FormDataParam("photoUrl") FormDataContentDisposition photoUrlMetadata){
		
		User authenticatedUser = getAuthenticatedUser(req);
		
		if (authenticatedUser != null) {
		
			String fileName = FileService.writeFileBytes(photoUrl, photoUrlMetadata);
			
			Book book = new Book();
			
			
			book.setTitle(title);
			book.setAuthor(author);
			book.setIsbn(isbn);
			book.setPublisher(publisher);
			book.setPublishingYear(publishingYear);
			book.setPageCount(pageCount);
			book.setWeight(weight);
			book.setLanguage(language);
			book.setDescription(description);
			book.setDimensions(dimensions);
			book.setPhotoUrl(fileName);
			book.setOwnerId(authenticatedUser.getId());
			book.setFee(fee);
			//book.setOwner(authenticatedUser);
			BookService.addBook(book);
			return "OK";
		} else {return null;}
	}
	
//	@POST
//	@Path("/edit_book")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.TEXT_PLAIN)
//	public String editBook(Book book) {
//		BookService.editBook(book);
//		return "OK";
//	}
	
	@POST
	@Path("/edit_book")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public String editBook(
			@Context HttpServletRequest req,
			@FormDataParam("id") int id,
			@FormDataParam("isbn") String isbn, 
			@FormDataParam("title") String title,
			@FormDataParam("author") String author, 
			@FormDataParam("publisher") String publisher,
			@FormDataParam("publishingYear") int publishingYear,
			@FormDataParam("pageCount") int pageCount,
			@FormDataParam("weight") double weight, 
			@FormDataParam("dimensions") String dimensions,
			@FormDataParam("fee") double fee, 
			@FormDataParam("description") String description, 
			@FormDataParam("langauge") String language,
			@FormDataParam("photoUrl") InputStream photoUrl,
			@FormDataParam("photoUrl") FormDataContentDisposition photoUrlMetadata) {

		User authenticatedUser = getAuthenticatedUser(req);
		
		if (authenticatedUser != null) {
		
			String fileName = FileService.writeFileBytes(photoUrl, photoUrlMetadata);
			
			Book book = new Book();
			
			book.setId(id);
			book.setTitle(title);
			book.setAuthor(author);
			book.setIsbn(isbn);
			book.setPublisher(publisher);
			book.setPublishingYear(publishingYear);
			book.setPageCount(pageCount);
			book.setWeight(weight);
			book.setLanguage(language);
			book.setDescription(description);
			book.setDimensions(dimensions);
			book.setPhotoUrl(fileName);
			book.setOwnerId(authenticatedUser.getId());
			book.setFee(fee);
			//book.setOwner(authenticatedUser);
			BookService.editBook(book);
			
			return "OK";
			
		} else {return null;}
	}
	
	@POST
	@Path("/delete_book")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteBook(@FormParam("book_id") int bookId) {
		BookService.deleteBook(bookId);
		return "OK";
	}
	@POST
	@Path("/authenticate_user")
	@Produces(MediaType.TEXT_PLAIN)
	public String authenticateUser(@Context HttpServletRequest req, @FormParam("email") String email,
			@FormParam("password") String password) {
		User user = AuthenticationService.getUser(email, password);
		if (user == null) {
			// Autentimine ebaõnnestus.
			return "FAIL";
		} else {
			// Autentimine õnnestus.
			HttpSession session = req.getSession(true);
			session.setAttribute("AUTH_USER", user);
			return "SUCCESS";
		}
	}
	
	@GET
	@Path("/get_authenticated_user")
	@Produces(MediaType.APPLICATION_JSON)
	public User getAuthenticatedUser(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja on sisse loginud.
			return (User)session.getAttribute("AUTH_USER");
		} else {
			// Kasutaja ei ole sisse loginud.
			return new User(); // Kasutame tühja objekti.
		}
	}
	
	@GET
	@Path("/logout")
	@Produces(MediaType.TEXT_PLAIN)
	public String logout(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		session.removeAttribute("AUTH_USER");
		return "SUCCESS";
	}

	@GET
	@Path("/get_image")
	@Produces("image/*")
	public Response getImage(@QueryParam("file_name") String fileName) throws IOException {
		byte[] imageBytes = FileService.readFileBytes(fileName);
		return Response.ok(imageBytes).build();
	}

	@POST
	@Path("/return_book")
	@Produces(MediaType.TEXT_PLAIN)
	public String returnBook(@FormParam("book_id") int bookId) {
		BookService.returnBook(bookId);
		return "OK";
	}
	
	@POST
	@Path("/add_user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addUser(User user) {
		UserService.addUser(user);
		return "OK";
	}
	
	
}














