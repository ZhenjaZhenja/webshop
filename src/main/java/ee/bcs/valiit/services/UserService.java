package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ee.bcs.valiit.model.Book;
import ee.bcs.valiit.model.User;

public class UserService {

	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/webshop";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";
	
	public static ResultSet executeSql(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void addUser( User user) {
		
		String sql = "INSERT INTO webshop.user (first_name, last_name, username, password, city, ssid, address, phone_number, email) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s')";
		sql = String.format(sql, user.getFirstName(), user.getLastName(), user.getUsername(), user.getPassword(), user.getCity(), user.getSsid(), user.getAddress(), user.getPhoneNumber(),
				user.getEmail());
		executeSql(sql);
	}

	public static User getUserDetails(int userId) {
		try {
			String sql = "SELECT id, first_name, last_name, username, city FROM webshop.user WHERE id=" + userId;
			ResultSet result = executeSql(sql);
			if (result != null) {
				if (result.next()) {
					User user = new User();
					user.setId(result.getInt("id"));
					user.setFirstName(result.getString("first_name"));
					user.setLastName(result.getString("last_name"));
					user.setUsername(result.getString("username"));
					user.setCity(result.getString("city"));
					return user;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
