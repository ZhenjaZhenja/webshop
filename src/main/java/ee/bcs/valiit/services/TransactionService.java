package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.valiit.model.Book;
import ee.bcs.valiit.model.Transaction;

public class TransactionService {
	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/webshop";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSql(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	


public static List<Transaction> getMyBorrowedBooks(int borrowerId) {
	List<Transaction> borrowedBooks = new ArrayList<Transaction>();
	try {
		ResultSet result = executeSql("select * from webshop.order where borrower_id =" + borrowerId);
		if (result != null) {
			while (result.next()) {
				Transaction borrowedBook = new Transaction();
				borrowedBook.setId(result.getInt("id"));
				borrowedBook.setBookId(result.getInt("book_id"));
				borrowedBook.setOwnerId(result.getInt("owner_id"));
				borrowedBook.setBorrowerId(result.getInt("borrower_id"));
				borrowedBook.setLendingDate(result.getString("lending_date"));
				borrowedBook.setReturnDate(result.getString("return_date"));
				borrowedBook.setBook(BookService.getBook(borrowedBook.getBookId()));
				borrowedBook.setBorrower(UserService.getUserDetails(borrowedBook.getBorrowerId()));
				borrowedBook.setOwner(UserService.getUserDetails(borrowedBook.getOwnerId()));
				borrowedBooks.add(borrowedBook);
			}
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return borrowedBooks;
}

public static void addOrder(Transaction transaction) {
	
	String sql = "INSERT INTO webshop.order (book_id, owner_id, borrower_id, return_date, lending_date) VALUES (%s, %s, %s, '%s', '%s');";
	sql = String.format(sql, transaction.getBookId(), transaction.getOwnerId(), transaction.getBorrowerId(), transaction.getReturnDate(), transaction.getLendingDate());
	executeSql(sql);
	
}

public static void updateOrder(int bookId) {
	

	String sql2 = "UPDATE webshop.book SET availability = 'Unavailable' WHERE id = " + bookId +";";
	executeSql(sql2);
}

}