package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.valiit.model.Book;
import ee.bcs.valiit.model.User;



public class BookService {
	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/webshop";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSql(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<Book> getBooks() {
		List<Book> books = new ArrayList<Book>();
		try {
			ResultSet result = executeSql("select * from webshop.book where availability <> 'Deleted';");
			if (result != null) {
				while (result.next()) {
					Book book = new Book();
					book.setId(result.getInt("id"));
					book.setTitle(result.getString("title"));
					book.setAuthor(result.getString("author"));
					book.setIsbn(result.getString("isbn"));
					book.setPublisher(result.getString("publisher"));
					book.setPublishingYear(result.getInt("publishing_year"));
					book.setPageCount(result.getInt("page_count"));
					book.setWeight(result.getDouble("weight"));
					book.setLanguage(result.getString("language"));
					book.setDescription(result.getString("description"));
					book.setAvailability(result.getString("availability"));
					book.setDimensions(result.getString("dimensions"));
					book.setPhotoUrl(result.getString("photo_url"));
					book.setOwnerId(result.getInt("owner_id"));
					book.setFee(result.getDouble("fee"));
					books.add(book);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return books;
	}
	
	//funktsioon My_books leheküljele, et kuvada kasutaja poolt süsteemi lisatud raamatud
	public static List<Book> getMyBooks(int ownerId) {
		List<Book> books = new ArrayList<Book>();
		try {
			ResultSet result = executeSql("select * from webshop.book where availability != 'Deleted' and owner_id =" + ownerId);
			if (result != null) {
				while (result.next()) {
					Book book = new Book();
					book.setId(result.getInt("id"));
					book.setTitle(result.getString("title"));
					book.setAuthor(result.getString("author"));
					book.setIsbn(result.getString("isbn"));
					book.setPublisher(result.getString("publisher"));
					book.setPublishingYear(result.getInt("publishing_year"));
					book.setPageCount(result.getInt("page_count"));
					book.setWeight(result.getDouble("weight"));
					book.setLanguage(result.getString("language"));
					book.setDescription(result.getString("description"));
					book.setAvailability(result.getString("availability"));
					book.setDimensions(result.getString("dimensions"));
					book.setPhotoUrl(result.getString("photo_url"));
					book.setOwnerId(result.getInt("owner_id"));
					book.setFee(result.getDouble("fee"));
					books.add(book);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return books;
	}
	
	
	
	
	public static void addBook(Book book) {
		
			String sql = "INSERT INTO webshop.book (isbn, title, author, publisher, publishing_year, page_count, weight, dimensions, language, photo_url, description, owner_id, fee, availability) VALUES ('%s', '%s', '%s', '%s', %s, %s, %s, '%s', '%s', '%s', '%s', %s, %s, '%s')";
			sql = String.format(sql, book.getIsbn(), book.getTitle(), book.getAuthor(), book.getPublisher(), book.getPublishingYear(), book.getPageCount(), book.getWeight(), book.getDimensions(),
					book.getLanguage(), book.getPhotoUrl(), book.getDescription(), book.getOwnerId(), book.getFee(), book.getAvailability());
			executeSql(sql);
		}
	
	public static void editBook(Book book) {
		String sql="";
		if (book.getPhotoUrl() != null && book.getPhotoUrl().length() > 0) {
		sql = String.format("UPDATE webshop.book SET isbn='%s', title='%s', author='%s', publisher='%s', publishing_year=%s, page_count=%s, weight=%s, dimensions='%s', language='%s', photo_url='%s', description='%s', owner_id=%s, fee=%s, availability='%s' WHERE id = %s", book.getIsbn(), book.getTitle(), book.getAuthor(), book.getPublisher(), book.getPublishingYear(), book.getPageCount(), book.getWeight(), book.getDimensions(),
				book.getLanguage(), book.getPhotoUrl(), book.getDescription(), book.getOwnerId(), book.getFee(), book.getAvailability(), book.getId());
		} else
		{sql = String.format("UPDATE webshop.book SET isbn='%s', title='%s', author='%s', publisher='%s', publishing_year=%s, page_count=%s, weight=%s, dimensions='%s', language='%s', description='%s', owner_id=%s, fee=%s, availability='%s' WHERE id = %s", book.getIsbn(), book.getTitle(), book.getAuthor(), book.getPublisher(), book.getPublishingYear(), book.getPageCount(), book.getWeight(), book.getDimensions(),
				book.getLanguage(), book.getDescription(), book.getOwnerId(), book.getFee(), book.getAvailability(), book.getId());}
		executeSql(sql);
		
	}
	
	public static Book getBook(int bookId) {
		try {
			String sql = "select * from webshop.book where availability != 'Deleted' and id=" + bookId;
			ResultSet result = executeSql(sql);
			if (result != null) {
				if (result.next()) {
					Book book = new Book();
					book.setId(result.getInt("id"));
					book.setTitle(result.getString("title"));
					book.setAuthor(result.getString("author"));
					book.setIsbn(result.getString("isbn"));
					book.setPublisher(result.getString("publisher"));
					book.setPublishingYear(result.getInt("publishing_year"));
					book.setPageCount(result.getInt("page_count"));
					book.setWeight(result.getDouble("weight"));
					book.setLanguage(result.getString("language"));
					book.setDescription(result.getString("description"));
					book.setAvailability(result.getString("availability"));
					book.setDimensions(result.getString("dimensions"));
					book.setPhotoUrl(result.getString("photo_url"));
					book.setOwnerId(result.getInt("owner_id"));
					book.setFee(result.getDouble("fee"));
					book.setOwner(getBookOwnerDetails(book.getOwnerId()));
					return book;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static User getBookOwnerDetails(int bookOwnerId) {
		try {
			String sql = "SELECT first_name, last_name, username, city FROM webshop.user inner join webshop.book on webshop.user.id=webshop.book.owner_id where webshop.book.owner_id=" + bookOwnerId;
			ResultSet result = executeSql(sql);
			if (result != null) {
				if (result.next()) {
					User owner = new User();
					owner.setFirstName(result.getString("first_name"));
					owner.setLastName(result.getString("last_name"));
					owner.setUsername(result.getString("username"));
					owner.setCity(result.getString("city"));
					return owner;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void deleteBook(int bookId) {
		String sql = "UPDATE webshop.book SET availability = 'Deleted' WHERE id = " + bookId +";";
		executeSql(sql);
		
	}

	public static void returnBook(int bookId) {
		String sql = "UPDATE webshop.book SET availability = 'Available' WHERE id = " + bookId +";";
		executeSql(sql);
		
	}
	}

