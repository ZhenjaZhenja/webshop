CREATE DATABASE  IF NOT EXISTS `webshop` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `webshop`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: webshop
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `publisher` varchar(450) DEFAULT NULL,
  `publishing_year` int(11) DEFAULT NULL,
  `page_count` int(11) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `dimensions` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `photo_url` varchar(300) DEFAULT NULL,
  `description` varchar(1045) DEFAULT NULL,
  `owner_id` int(11) NOT NULL,
  `fee` decimal(5,2) NOT NULL,
  `availability` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_owner_idx` (`owner_id`),
  CONSTRAINT `book_owner` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (2,'9780120000000','Basic Neurochemistry : Principles of Molecular, Cellular, and Medical Neurobiology','Scott Brady , Donald Price , R. Wayne Albers , George J. Siegel','Elsevier Science Publishing Co Inc',2011,1120,997,'157.5 x 231.1 x 30.5','english','9780123749475.jpg','Basic Neurochemistry, Eighth Edition, is the updated version of the outstanding and comprehensive classic text on neurochemistry. For more than forty years, this text has been the worldwide standard for information on the biochemistry of the nervous system, serving as a resource for postgraduate trainees and teachers in neurology, psychiatry, and basic neuroscience, as well as for medical, graduate, and postgraduate students and instructors in the neurosciences.',1,5.00,'Available'),(4,'9780200000000','Oxford IB Diploma Programme: Mathematics Standard Level Course Companion : Oxford Ib Diploma Program','Paul La Rondie, Jim Fensom, Ed Kemp, Laurie Buchanan, Jill Stevens','Oxford University Press',2015,800,1610,'193.04 x 254 x 35.56','english','9780198390114.jpg','With unrivalled guidance straight from the IB, over 700 pages of practice and the most comprehensive and correct syllabus coverage, this course book will set your learners up to excel. The only resource developed directly with the IB, it fully captures the IB ethos, connecting mathematical applications and practice with inquiry.',1,8.00,'Available'),(5,'9781410000000','Contemporary Linguistics : An Introduction','John Archibald, Francis Katamba, William O\'Grady','Pearson Education Limited',2011,720,1360,'189 x 246 x 36','english','9781405899307.jpg','Contemporary Linguistics can be used from first year through to final year as a main text for students taking degree courses in linguistics, English language and cognitive science and by MA students on TEFL courses. It is also highly suitable for students taking language options in media and cultural studies, modern language, psychology and philosophy, as well as for speech therapy courses.Contemporary Linguistics : An introduction is a comprehensive, fully up-to-date introduction to linguistics. The book covers not only how language is structured, but also how it functions both socially and culturally, and how it is acquired and processed by speakers. It will prepare students to go on to more advanced work and, at the same time, will serve as a basic reference that students can continue to consult throughout their studies.',1,3.00,'Available'),(6,'9780340000000','An Introduction to Applied Linguistics','Norbert Schmitt','Taylor & Francis Ltd',2010,352,590,'154.94 x 231.14 x 17.78','english','9780340984475.jpg','An Introduction to Applied Linguistics, Second Edition provides a complete, authoritative and up-to-date overview of the state of the field. Each of the 15 chapters offers an extended survey of a central element of Applied Linguistics and is co-authored by two leading international specialists, thus ensuring a full and balanced treatment of the topic covered. The book is divided into three sections: a description of language and language use; essential areas of enquiry; and the four skills and testing. An Introductory chapter familiarises readers with key issues and recurrent themes whilst hands-on activities and further reading sections for each chapter encourage practical analysis and wider reading.',1,6.00,'Available'),(7,'9780720000000','Color Atlas of Veterinary Anatomy, Volume 3, The Dog and Cat','Peter C. Goody, Stanley H. Done, Susan A. Evans, Neil C. Stickland','Elsevier Health Sciences',2009,540,2440,'250 x 304 x 32','english','9780723434153.jpg','If you are looking for a book that presents a unique photographic record of dissections showing the topographical anatomy of the dog and cat: this is the atlas for you! Part of a comprehensive 3-volume set that also covers Ruminants (vol 1) and The Horse (vol 2), the Color Atlas of the Dog and Cat takes a complete look at virtually every aspect of veterinary anatomy. With this book you will be able to see the position and relationships of bones, muscles, nerves, blood vessels and viscera that go to make up each region of the body and each organ system.',1,3.00,'Available'),(8,'9780320000000','Dyce, Sack, and Wensing\'s Textbook of Veterinary Anatomy','Baljit Singh','Elsevier - Health Sciences Division',2017,872,2472,'222 x 281 x 33.02','english','9780323442640.jpg','Gain the working anatomic knowledge that is crucial to your understanding of the veterinary basic sciences with Textbook of Veterinary Anatomy, 5th Edition. By focusing on the essential anatomy of each species, this well-established book details information directly applicable to the care of dogs, cats, horses, cows, pigs, sheep, goats, birds, and camelids - and points out similarities and differences among species. Each chapter includes a conceptual overview that describes the structure and function of an anatomic region, and new diagrams facilitate comprehension of bodily functions.',1,10.00,'Unavailable'),(9,'9781290000000','Corporate Finance, Global Edition','Jonathan Berk, Peter DeMarzo','Pearson Education Limited',2016,1168,2130,'204 x 254 x 41','english','9781292160160.jpg','Using the unifying valuation framework based on the Law of One Price, top researchers Jonathan Berk and Peter DeMarzo have set the new canon for corporate finance textbooks. Corporate Finance, Fourth Edition blends coverage of time-tested principles and the latest advancements with the practical perspective of the financial manager, so students have the knowledge and tools they need to make sound financial decisions in their careers.',1,10.00,'Available'),(10,'9780730000000','Psychology','Lorelle Jane Burton, Drew Westen, Robin M. Kowalski','John Wiley & Sons Australia Ltd',2014,1000,2162,'235 x 277 x 34','english','9780730304685.jpg','Building on the success of the market-leading and award-winning previous editions, Psychology: 4th Australian and New Zealand Edition has been thoroughly updated to provide comprehensive coverage of contemporary local and international data, research and examples in the dynamic field of psychology.',1,15.00,'Available'),(11,'9780210000000','Evolutionary Psychology : The New Science of the Mind','David Buss','Taylor & Francis Ltd',2014,496,862,'218.44 x 276.86 x 22.86','english','9780205992126.jpg','This book examines human psychology and behavior through the lens of modern evolutionary psychology. Evolutionary Psychology: The Ne w Science of the Mind, 5/e provides students with the conceptual tools of evolutionary psychology, and applies them to empirical research on the human mind. Content topics are logically arrayed, starting with challenges of survival, mating, parenting, and kinship; and then progressing to challenges of group living, including cooperation, aggression, sexual conflict, and status, prestige, and social hierarchies. Students gain a deep understanding of applying evolutionary psychology to their own lives and all the people they interact with.',1,8.00,'Available'),(12,'9781460000000','Clinical Handbook of Psychological Disorders, Fifth Edition : A Step-by-Step Treatment Manual','David H. Barlow','Guilford Publications',2014,768,1451,'190.5 x 236.22 x 43.18','english','9781462513260.jpg','This clinical reference and widely adopted text is recognized as the premier guide to understanding and treating frequently encountered psychological disorders in adults. Showcasing evidence-based psychotherapy models, the volume addresses the most pressing question asked by students and practitioners--How do I do it? Leading authorities present state-of-the-art information on each clinical problem and explain the conceptual and empirical bases of their respective therapeutic approaches. Procedures for assessment, case formulation, treatment planning, and intervention are described in detail. Extended case examples with session transcripts illustrate each component of treatment.',1,5.00,'Unavailable'),(13,'9780130000000','Art History','Marilyn Stokstad, Michael W. Cothren','Pearson Education (US)',2012,1248,3682,'228 x 280 x 57','english','9780134475882.jpg','Art History brings the history of art to life for a new generation of students. It is global in scope, inclusive in its coverage, and warm and welcoming in tone. The guiding vision of Art History is that the teaching of art history survey courses should be filled with equal delight, enjoyment, and serious learning, while fostering an enthusiastic and educated public for the visual arts. The Sixth Edition has been revised to reflect new discoveries, recent research, and fresh interpretive perspectives, as well as to address the changing needs of both students and educators.',1,3.00,'Available'),(14,'9780690000000','Animal Spirits : How Human Psychology Drives the Economy, and Why It Matters for Global Capitalism','George A. Akerlof , Robert J. Shiller','Princeton University Press',2011,264,299.37,'140 x 20 8x 24','english','9780691145921.jpg','The global financial crisis has made it painfully clear that powerful psychological forces are imperiling the wealth of nations today. From blind faith in ever-rising housing prices to plummeting confidence in capital markets, \'animal spirits\' are driving financial events worldwide. In this book, acclaimed economists George Akerlof and Robert Shiller challenge the economic wisdom that got us into this mess, and put forward a bold new vision that will transform economics and restore prosperity.',3,3.00,'Available'),(16,'9780390000000','The Elements of Statistical Learning: Data Mining, Inference, and Prediction, Second Edition (Springer Series in Statistics)','Trevor Hastie, Robert Tibshirani, Jerome Friedman','Springer',0,745,1361,'236 x 163 x 36','null','c1c622b6-c245-46cb-8cbc-3fb5e06dfe49.jpg','During the past decade there has been an explosion in computation and information technology. With it have come vast amounts of data in a variety of fields such as medicine, biology, finance, and marketing. The challenge of understanding these data has led to the development of new tools in the field of statistics, and spawned new areas such as data mining, machine learning, and bioinformatics. Many of these tools have common underpinnings but are often expressed with different terminology. This book describes the important ideas in these areas in a common conceptual framework.',2,8.00,'Unavailable'),(17,'97899500000001','Tõenäosusteooria algkursus','Kalev Pärna','null',0,209,0,'250 x 175','estonian','1362487652.png','Õpik on kirjutatud elementaarse sissejuhatusena tõenäosusteooriasse, pidades silmas erinevate erialade üliõpilaste vajadusi.',2,3.00,'Available'),(18,'9789950000000','Intelligentsuse psühholoogia','Anu Realo, Jüri Allik','TÜ Kirjastus',2011,432,NULL,NULL,'estonian','1308042812.png','Raamatus kirjeldatakse, kuidas intelligentsus areneb ja aja jooksul muutub. Põhjalikult võetakse luubi alla see, millist mõju intelligentsuse tase inimeste elukäigule avaldab.',2,3.00,'Available'),(19,'9789950000000','Finantsturud ja -institutsioonid','Nadežda Ivanova, Maire Nurmet, Andro Roos, Priit Sander','TÜ Kirjastus',2014,430,NULL,'210 x 140','estonian','9789949199808.jpg','Käesoleva õpiku eesmärgiks on raha ja rahapoliitika, finantsturgude ning pankade toimimisega seotud olulisemate teoreetiliste aspektide kompleksne tundmaõppimine, mis on eduka praktilise tegevuse aluseks.',2,5.00,'Unavailable'),(20,'9789950000000','Estonian Approaches to Culture Theory','Valter Lang, Kalevi Kull','TÜ Kirjastus',2014,335,60,'null','null','bc8c3a0e-2611-4047-a8d4-6a68d5603ac6.png','The fourth volume in the Approaches to Culture Theory series is a contemporary Estonian anthology in culture theory.',2,6.00,'Available'),(50,'9780120000000','Basic Neurochemistry : Principles of Molecular, Cellular, and Medical Neurobiology','Scott Brady , Donald Price , R. Wayne Albers , George J. Siegel','Elsevier Science Publishing Co Inc',2011,1120,997,'157.5 x 231.1 x 30.5','english','9780123749475.jpg','Basic Neurochemistry, Eighth Edition, is the updated version of the outstanding and comprehensive classic text on neurochemistry. For more than forty years, this text has been the worldwide standard for information on the biochemistry of the nervous system, serving as a resource for postgraduate trainees and teachers in neurology, psychiatry, and basic neuroscience, as well as for medical, graduate, and postgraduate students and instructors in the neurosciences.',1,5.00,'Available'),(51,'9780690000000','The Princeton Companion to Mathematics','Imre Leader , Timothy Gowers , June Barrow-Green','Princeton University Press',2015,1056,2620,'212 x 254 x 68','english','9780691118802.jpg','This is a one-of-a-kind reference for anyone with a serious interest in mathematics. Edited by Timothy Gowers, a recipient of the Fields Medal, it presents nearly two hundred entries, written especially for this book by some of the world\'s leading mathematicians, that introduce basic mathematical tools and vocabulary; trace the development of modern mathematics; explain essential terms and concepts; examine core ideas in major areas of mathematics; describe the achievements of scores of famous mathematicians; explore the impact of mathematics on other disciplines such as biology, finance, and music--and much, much more.',1,5.00,'Available');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_category`
--

DROP TABLE IF EXISTS `book_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_category` (
  `book_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  KEY `book_category_book_idx` (`book_id`),
  KEY `book_category_category_idx` (`category_id`),
  CONSTRAINT `book_category_book` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `book_category_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_category`
--

LOCK TABLES `book_category` WRITE;
/*!40000 ALTER TABLE `book_category` DISABLE KEYS */;
INSERT INTO `book_category` VALUES (2,1),(2,2),(2,5),(4,3),(5,14),(6,14),(7,6),(8,6),(9,4),(10,12),(11,12),(12,12),(13,11),(14,12),(14,10),(17,3),(18,12),(19,10),(20,11);
/*!40000 ALTER TABLE `book_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'chemistry'),(2,'biology'),(3,'mathematics'),(4,'finance'),(5,'medicine'),(6,'veterinary'),(7,'physics'),(8,'philosophy'),(9,'marketing'),(10,'economics'),(11,'art'),(12,'psychology'),(13,'computer science'),(14,'linguistics');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `borrower_id` int(11) NOT NULL,
  `return_date` varchar(15) NOT NULL,
  `lending_date` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_borrower_idx` (`borrower_id`),
  KEY `order_owner_idx` (`owner_id`),
  KEY `order_book_idx` (`book_id`),
  CONSTRAINT `order_book` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_borrower` FOREIGN KEY (`borrower_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_owner` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,16,2,3,'2018-06-20','2018-02-20'),(2,4,1,2,'2018-07-19','2018-05-25'),(3,7,1,2,'2018-04-15','2018-08-17');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `ssid` varchar(45) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Jevgenia','Bredihhina','Zhenja','tere','Tartu','48888888888','Kaunase pst. 23-6',53089914,'jevgenia.bredihhina@gmail.com'),(2,'Liis','Jürisoo','Liis','tere','Pärnu','49999999999','Tallinna mnt. 12-56',53089915,'liis@gmail.com'),(3,'Aleksandra','Kerb','Sasha','tere','Rakvere','48888888888','Haljala mnt. 8-5',53089916,'aleksandra.kerb@gmail.com'),(4,'null','B','AB','tere','Pärnu','null','Tallinna mnt',55005511,'ab@gmail.com'),(5,'as','as','as','fgfg','as','675676776','as',5675676,'as@gmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 10:52:13
