function logInUser(){
	var email = $('#email').val();
	var password = $('#pwd').val();
	
	$.ajax({
		url: '/webshop/rest/authenticate_user',
		method: 'POST',
		data: {
			'email': email,
			'password': password
			},
		complete: function (result){
			//logimisprotseduur lõppenud
			if(result.responseText == 'SUCCESS') {
				//alert("Sisselogimine ebaõnnestus!");
				document.location = 'my_books.html';
			} else if (!(result.responseText == 'SUCCESS')){  
				alert('error');
				
			} 
		}
	});
}

function logInUser2(){
	var email2 = $('#email2').val();
	var password2 = $('#pwd2').val();
	
	$.ajax({
		url: '/webshop/rest/authenticate_user',
		method: 'POST',
		data: {
			'email': email2,
			'password': password2
			},
		complete: function (result){
			//logimisprotseduur lõppenud
			if(result.responseText == 'SUCCESS') {
				//alert("Sisselogimine ebaõnnestus!");
				document.location = 'my_books.html';
			} else if (!(result.responseText == 'SUCCESS')){  
				alert('error');
				
			}
		}
	});
}

function logOutUser(){
	$.ajax(
			{
				url: '/webshop/rest/logout',
				method: 'GET',
				complete: function (result) {
					$('#logInButton').show();
					$('#logOutButton').hide();
					document.location = 'books2.html';
					//$('#errorBox').hide();
				}
			}
		);

}


function getAuthenticatedUser(){
	
	$.ajax({
		url: '/webshop/rest/get_authenticated_user',
		method: 'GET',
		complete: function (result){
			var user = result.responseJSON;
			if (user != null && user.id > 0){
				$('#logOutButton').show();
			} else {//kasutaja ei ole sisse logitud ja kuvame login nupu
				 $('#logInButton').show();
				 $('#logOutButton').hide();
				}
			//return user.id;
		}
	});
	
}