/**
 Javascript functions library 
 */
function getUrlParameter(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	var results = regex.exec(location.search);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g,
			' '));
};

function giveMeAllTheBooks() {
	// Teeme AJAX päringu raamatute pärimiseks Java EE rakendusest.
	$.ajax({
		method : "GET",
		url : "/webshop/rest/get_books",
		dataType : "json",
		contentType : 'application/json',
		complete : function(response) {
			$('#books_list').empty();
			books = response.responseJSON;
			displayBooks($('#mainSearch').val());
		}
	});
}

function displayBooks(searchText) {

	$('#books_list').empty();

	for (var i = 0; i < books.length; i++) {

		if (books[i].title.toUpperCase().includes(searchText.toUpperCase())
				|| books[i].author.toUpperCase().includes(
						searchText.toUpperCase())) {
			var bookRow = '<div class="col-lg-3 col-md-6 col-sm-12 d-flex align-items-stretch">';
			bookRow = bookRow
					+ '<div class="card mb-3 box-shadow"><img class="card-img-top" src="/webshop/rest/get_image?file_name=' + books[i].photoUrl +'" alt="Card image cap">';
			bookRow = bookRow + '<div class="card-body">';
			bookRow = bookRow + '<h4 class="card-text">' + books[i].title
					+ '</h4>';
			bookRow = bookRow + '<p class="card-text">' + books[i].author
					+ '</p>';
			bookRow = bookRow
					+ '<p class="card-text" style="font-size: 20px;">'
					+ books[i].fee + ' EUR</p></br>';
			bookRow = bookRow
					+ '<div class="btn-group" style="position: absolute; bottom: 10px; top: auto" >';
			bookRow = bookRow
					+ '<button type="button" class="btn btn-sm btn-outline-success" style="position: relative" onClick ="giveMeTheBook('
					+ books[i].id + ');">book details</button>';
			if(books[i].availability != "Unavailable"){
			bookRow = bookRow
			+ '<button type="button" class="btn btn-sm btn-outline-success" style="position: relative" onClick ="borrowBook('
			+ books[i].id+","+books[i].ownerId + ');">borrow book</button>';}
			bookRow = bookRow + '</div></div></div></div></div>';
			$('#books_list').append(bookRow);
		}
	}
}




// My books lehe funktsioonid
// Ebaturvaline lahendus

// Kasutaja raamatute cardide koodi genereerimine
function displayMyBooks() {
	$('#container-fluid').empty();
	for (var i = 0; i < my_books.length; i++) {
		
		
			var bookRow = '<div class="row" id="my_books" style="margin-left:30px;"><div class="card w-75">';
			bookRow= bookRow +'<div class="card-body"><div class="row d-flex align-items-stretch">';
			//bookRow= bookRow +'<div class="col-sm-4"><img class="card-img" src="' + my_books[i].photoUrl +'" alt="Card image cap"></div>';
			bookRow= bookRow +'<div class="col-sm-4"><img class="card-img" src="/webshop/rest/get_image?file_name=' + my_books[i].photoUrl +'" alt="Card image cap"></div>';
			bookRow= bookRow +'<div class="col-sm-6">';
			bookRow= bookRow +'<div id="title"><strong>Title: </strong>'+ my_books[i].title + '</div></br>';
			bookRow= bookRow +'<div id="author"><strong>Author: </strong>'+my_books[i].author+'</div></br>';
			bookRow= bookRow +'<div id="ISBN"><strong>ISBN: </strong>'+my_books[i].isbn+'</div></br>';
			bookRow= bookRow +'<div id="status"><strong>Availability: </strong>'+my_books[i].availability+'</div></div>';
			bookRow= bookRow +'<div class="btn-group-vertical col-sm-2" style="position: right">';
			bookRow= bookRow +'<button type="button" class="btn btn-info" onClick ="giveMeTheBook('
			+ my_books[i].id + ');">Details</button></br>';
			bookRow= bookRow +'<button type="button" class="btn btn-info" onClick ="prepareMyBookEdit('
			+ my_books[i].id + ');">Edit</button></br>';
			bookRow= bookRow +'<button type="button" class="btn btn-info" onClick ="deleteBook('
			+ my_books[i].id + ');">Delete</button></br>';
			
			if(my_books[i].availability == "Unavailable"){
			bookRow= bookRow +'<button type="button" class="btn btn-info" onClick ="returnBook('
			+ my_books[i].id + ');">Returned</button></div></div></div></div></br>';}
			
			$('#container-fluid').append(bookRow);
		}

}



function getMyBooks(activeUserId) {
	
	$.ajax({
		method : "GET",
		url : "/webshop/rest/get_my_books?owner_id=" + activeUserId,
		dataType : "json",
		contentType : 'application/json',
		complete : function(response) {
			//$('#container-fluid').empty();
			my_books = response.responseJSON;
			displayMyBooks();
		}
	});
}

function getUsersBooks(){
	
	$.ajax({
		url: '/webshop/rest/get_authenticated_user',
		method: 'GET',
		complete: function (result){
			user = result.responseJSON;			
			if (user != null && user.id > 0){
				$('#addBook').show();
				$('#myBooksContainer').show();	
				getMyBooks(user.id);
				displayMyTransactions(user.id);
			} else {$('#sign-in-box').show();}
		}

	});
	
}



function deleteBook(bookId) { 
	if(confirm("Are you sure, you want to delete this book?")) {
		$.ajax(
				{
					method: "POST",
					url: "/webshop/rest/delete_book",
					data: 					
						{
							"book_id": bookId
						},
					complete: function (response) {
						getUsersBooks();
					}
				}
		);
	}
}

// Märgi raamat tagastatuks
function returnBook(bookId) { 	
		$.ajax(
				{
					method: "POST",
					url: "/webshop/rest/return_book",
					data: 					
						{
							"book_id": bookId
						},
					complete: function (response) {
						getUsersBooks();
					}
				}	
		);
	
}

function initMyUiControls() {
	for (i = new Date().getFullYear(); i > 1900; i--) {
		$("#yearpicker").append($('<option />').val(i).html(i));
	}
}

function saveMyNewBook() {

	var saveUrl = "";
	if ($('#book_id_field').val() > 0) {
		saveUrl = "/webshop/rest/edit_book";
	} else {
		saveUrl = "/webshop/rest/add_book";
	}
	$.ajax({
		method : "POST",
		url : saveUrl,
		dataType : "json",
		data : new FormData($('form')[4]),
		cache : false,
		contentType : false,
		processData : false,
		complete : function(response) {
			getUsersBooks();

		}

	});
}


function prepareMyBookEdit(bookId) {
	$.ajax({
		method : "GET",
		url : "/webshop/rest/get_book_details?book_id=" + bookId,
		datatype : "json",
		contentType : 'application/json',
		complete : function(response) {
			var book = response.responseJSON;
			$('#book_id_field').val(book.id);
			$('#book_isbn_textbox').val(book.isbn);
			$('#book_title_textbox').val(book.title);
			$('#book_author_textbox').val(book.author);
			$('#book_publisher_textbox').val(book.publisher);
			$('#book_publishingYear_textbox').val(book.publishingYear);
			$('#book_pageCount_textbox').val(book.pageCount);
			$('#book_weight_textbox').val(book.weight);
			$('#book_dimensions_textbox').val(book.dimensions);
			$('#book_language_textbox').val(book.language);
			$('#book_photoUrl_textbox').val("");
			$('#modalLogo').attr(
					"src",
					"/webshop/rest/get_image?file_name="
							+ book.photoUrl);
			$('#modalLogo').show();
			$('#book_description_textbox').val(book.description);
			//ownerId: $('#book_id_textbox').val("");
			$('#book_fee_textbox').val(book.fee);
			$('#bookModal').modal('show');
		}
	});
}

function prepareMyBookAdd() {
	$('#book_id_field').val("0");
	$('#book_isbn_textbox').val("");
	$('#book_title_textbox').val("");
	$('#book_author_textbox').val("");
	$('#book_publisher_textbox').val("");
	$('#book_publishingYear_textbox').val("");
	$('#book_pageCount_textbox').val("");
	$('#book_weight_textbox').val("");
	$('#book_dimensions_textbox').val("");
	$('#book_language_textbox').val("");
	$('#book_photoUrl_textbox').val("");
	$('#book_description_textbox').val("");
	//ownerId: $('#book_id_textbox').val("");
	$('#book_fee_textbox').val("");
	$('#modalLogo').hide();
	$('#bookModal').modal("show");
}
			


// Navbar + kasutaja funktsioonid 

function getUsersName(){
	
	$.ajax({
		url: '/webshop/rest/get_authenticated_user',
		method: 'GET',
		complete: function (result){
			user = result.responseJSON;			
			if (user != null && user.id > 0){
				
				var userName = '<i class="fa fa-smile-o"></i> ' + user.firstName + " " + user.lastName;
				
				$('#active-user').append(userName);
				}
		}

	});
	
}

function saveNewUser() {
	$.ajax({
		method : "POST",
		url : "/webshop/rest/add_user",
		dataType : "json",
		contentType : 'application/json',
		data : JSON.stringify({
			firstName : $('#firstName').val(),
			lastName : $('#lastName').val(),
			username : $('#username').val(),
			password : $('#signUpPwd').val(),
			phoneNumber : $('#phoneNumber').val(),
			city : $('#city').val(),
			ssid : $('#ssid').val(),
			address : $('#address').val(),
			email : $('#signUpEmail').val()

		}),
		complete : function(response) {
//alert('Sign up was successful - please log in!');

		}

	});
}


// Esilehe funktsioonid


function giveMeTheBook(bookId) {
	/* var bookId = new URLSearchParams(document.location.search)
			.get("book_id"); */

	// Teeme AJAX päringu raamatute pärimiseks Java EE rakendusest.
	$.ajax({
		method : "GET",
		url : "/webshop/rest/get_book_details?book_id=" + bookId,
		dataType : "json",
		contentType : 'application/json',
		complete : function(response) {

			$('#book-details-table').empty();
			var book_details = response.responseJSON;

			var tableRow = "<tr><th>ISBN</th><td>" + book_details.isbn
					+ "</td></tr>";
			tableRow = tableRow + "<tr><th>Publisher</th><td>"
					+ book_details.publisher + "</td></tr>";
			tableRow = tableRow + "<tr><th>Publishing year</th><td>"
					+ book_details.publishingYear + "</td></tr>";
			tableRow = tableRow + "<tr><th>Page count</th><td>"
					+ book_details.pageCount + "</td></tr>";
			tableRow = tableRow + "<tr><th>Dimensions</th><td>"
					+ book_details.dimensions + "</td></tr>";
			tableRow = tableRow + "<tr><th>Weight</th><td>"
					+ book_details.weight + "</td></tr>";
			tableRow = tableRow + "<tr><th>Language</th><td>"
					+ book_details.language + "</td></tr>";
			$('#book-details-table').append(tableRow);

			$('#product-image').empty();
			var pictureUrl = '<img src="/webshop/rest/get_image?file_name=' + book_details.photoUrl +'" style="max-width: 300px;">';
			$('#product-image').append(pictureUrl);

			$('#book-title').empty();
			var bookTitle = "<p>" + book_details.title + "</p>";
			$('#book-title').append(bookTitle);

			$('#authors').empty();
			var bookAuthor = "<p>" + book_details.author + "</p>";
			$('#authors').append(bookAuthor);

			$('#description').empty();
			var bookDescription = "<p>" + book_details.description
					+ "</p><br/>";
			$('#description').append(bookDescription);
			$('#bookDetailsModal').modal('show');

		}
	});
}

function generalSearch() {
	$("#mainSearch").on(
			"keyup",
			function() {
				if (!document.location.href.toLowerCase().includes(
						'books2.html')) {
					document.location = 'books2.html?search_text='
							+ $('#mainSearch').val();
				} else {

					displayBooks($('#mainSearch').val());
				}

			});

	if (document.location.href.toLowerCase().includes('books2.html')) {
		$('#mainSearch').val(getUrlParameter("search_text"));
		giveMeAllTheBooks();
	}
}

// Laenutusega seotud funktsioonid

function displayMyTransactions(borrowerId) {
	
	$.ajax(
		{
			method: "GET",
			url: "/webshop/rest/get_my_transaction?borrower_id=" + borrowerId,
			dataType: "json",
			contentType: 'application/json',
			complete: function (response) {
				$('#transaction_list').empty();
				var transactions = response.responseJSON;

				for (var i = 0; i < transactions.length; i++) {
					
					var book = transactions[i].book;
					var owner = transactions[i].owner;
					
					var transactionRow = "<tr>";
					transactionRow = transactionRow + "<td>" + transactions[i].book.title + "</td>";
					transactionRow = transactionRow + "<td>" + book.author + "</td>";
					transactionRow = transactionRow + "<td>" + book.isbn + "</td>";
					transactionRow = transactionRow + "<td>" + owner.firstName + " "+ owner.lastName + "</td>";
					transactionRow = transactionRow + "<td>" + transactions[i].returnDate + "</td>";
					transactionRow = transactionRow + "<td>" + transactions[i].lendingDate + "</td>";
					
					transactionRow = transactionRow + "</tr>";
					
					$('#transaction_list').append(transactionRow);
					
					// See variant töötaks ka
//					var transactionRow = "<tr>";
//					transactionRow = transactionRow + "<td>" + transactions[i].book.title + "</td>";
//					transactionRow = transactionRow + "<td>" + transactions[i].book.author + "</td>";
//					transactionRow = transactionRow + "<td>" + transactions[i].book.isbn + "</td>";
//					transactionRow = transactionRow + "<td>" + transactions[i].owner.firstName + " "+ transactions[i].owner.lastName + "</td>";
//					transactionRow = transactionRow + "<td>" + transactions[i].returnDate + "</td>";
//					transactionRow = transactionRow + "<td>" + transactions[i].lendingDate + "</td>";
//					
//					transactionRow = transactionRow + "</tr>";
//					
//					$('#transaction_list').append(transactionRow);

				}
			}
		}
	);
}


function borrowBook(bookId, ownerId){
	
	$.ajax({
		url: '/webshop/rest/get_authenticated_user',
		method: 'GET',
		complete: function (result){
			user = result.responseJSON;	
			if (user != null && user.id > 0){
			 $.ajax(
					 {
					 method: "POST",
					 url: "/webshop/rest/add_order",
					 dataType: "json",
					 contentType: 'application/json', 
					 data : JSON.stringify({
							bookId : bookId,
							ownerId: ownerId,
							borrowerId : user.id,
							returnDate: '2018-05-25',
							lendingDate : '2018-01-01'
					 }),
					 complete: function (response){
						
					alert('Success! Your book will arrive soon');
					giveMeAllTheBooks();
					
					 }
					
					 }
					 );}
			else{
				document.location = 'my_books.html';
						
					 }
		}

	});
	
}




